<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\Roles;

class baseRoles extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        Roles::create([
            'name'          => 'admin', 
            'display_name'  => 'Administrator',
            'description'   => 'Администратор',
        ]);
        Roles::create([
            'name'          => 'moderator', 
            'display_name'  => 'Moderator',
            'description'   => 'Модератор',
        ]);
        Roles::create([
            'name'          => 'operator', 
            'display_name'  => 'Оperator',
            'description'   => 'Оператор',
        ]);
        Roles::create([
            'name'          => 'viewer', 
            'display_name'  => 'Viewer',
            'description'   => 'Зритель',
        ]);
        Roles::create([
            'name'          => 'guest', 
            'display_name'  => 'Guest',
            'description'   => 'Гость',
        ]);

    }
}
