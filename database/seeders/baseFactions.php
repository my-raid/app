<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\Faction;

class baseFactions extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Faction::create([
            'name'          => 'Баннереты',
            'url'           => 'banner-lords',
            'federation_id' => 1,
        ]);
        Faction::create([
            'name'          => 'Высшие эльфы',
            'url'           => 'high-elves',
            'federation_id' => 1,
        ]);
        Faction::create([
            'name'          => 'Священный орден',
            'url'           => 'sacred-order',
            'federation_id' => 1,
        ]);
        Faction::create([
            'name'          => 'Варвары',
            'url'           => 'barbarians',
            'federation_id' => 1,
        ]);

        Faction::create([
            'name'          => 'Племена огринов',
            'url'           => 'ogryn-tribes',
            'federation_id' => 2,
        ]);
        Faction::create([
            'name'          => 'Ящеролюды',
            'url'           => 'lizardmen',
            'federation_id' => 2,
        ]);
        Faction::create([
            'name'          => 'Оборотни',
            'url'           => 'skinwalkers',
            'federation_id' => 2,
        ]);
        Faction::create([
            'name'          => 'Орки',
            'url'           => 'orcs',
            'federation_id' => 2,
        ]);

        Faction::create([
            'name'          => 'Демоны',
            'url'           => 'demonspawn',
            'federation_id' => 3,
        ]);
        Faction::create([
            'name'          => 'Орды Нежити',
            'url'           => 'undead-hordes',
            'federation_id' => 3,
        ]);
        Faction::create([
            'name'          => 'Темные Эльфы',
            'url'           => 'dark-elves',
            'federation_id' => 3,
        ]);
        Faction::create([
            'name'          => 'Отступники',
            'url'           => 'knights-revenant',
            'federation_id' => 3,
        ]);

        Faction::create([
            'name'          => 'Дворфы',
            'url'           => 'dwarves',
            'federation_id' => 4,
        ]);
        Faction::create([
            'name'          => 'Воины Сумрака',
            'url'           => 'shadowkin',
            'federation_id' => 4,
        ]);
    }
}
