<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\Federation;

class baseFederations extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Federation::create([
            'name' => 'Телерийцы'
        ]);
        Federation::create([
            'name' => 'Пакт Гаэллана'
        ]);
        Federation::create([
            'name' => 'Падшие'
        ]);
        Federation::create([
            'name' => 'Нерессийский союз'
        ]);
    }
}
